<?php
namespace samizdam\ValueObjects\Random;

use samizdam\ValueObjects\SingleValueObjectInterface;

/**
 * simple value-object for generate random integers
 * @author samizdam
 *
 */
class Int implements SingleValueObjectInterface{
	
	protected $min = 0;
	protected $max = PHP_INT_MAX;
	
	public function __construct($min = 0, $max = PHP_INT_MAX){
		$this->min = (int) $min;
		$this->max = (int) $max;
	}

	public function getValue(){
		return rand($this->min, $this->max);
	}

	public function generateArray($number = 1){
		$randomArray = [];
		for($i = 0; $i++ < $number;){
			$randomArray[] = $this->getValue();
		}
		return $randomArray;
	}
}