<?php
namespace samizdam\ValueObjects;
/**
 * 
 * @author samizdam
 *
 */
interface SingleValueObjectInterface{
	public function getValue();
}