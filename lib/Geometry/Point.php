<?php
namespace samizdam\ValueObjects\Geometry;
/**
 * Base class for point in Cartesian system 
 * @author samizdam
 *
 */
class Point{
	
	/**
	 * 
	 * @var number
	 */
	protected $x;
	
	/**
	 * 
	 * @var number
	 */
	protected $y;
	
	/**
	 * 
	 * @param number $x
	 * @param number $y
	 */
	public function __construct($x = 0, $y = 0){
		$this->setX($x);
		$this->setY($y);
	}
	
	public function toArray(){
		return [
			'x' => $this->getX(), 
			'y' => $this->getY()
		];
	}
	
	/**
	 * 
	 * @param number $x
	 */
	public function setX($x){
		$this->x = (float) $x;
	}
	
	/**
	 * 
	 * @param number $y
	 */
	public function setY($y){
		$this->y = (float) $y;
	}
	
	public function getX(){
		return $this->x;
	}
	
	public function getY(){
		return $this->y;
	}
	
	public function equals(Point $point){
		return ($this->x === $point->getX() && $this->y === $point->getY());
	}
}