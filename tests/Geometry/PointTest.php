<?php
use samizdam\ValueObjects\Geometry\Point;

class PointTest extends PHPUnit_Framework_TestCase{
	public function testPointSetGet(){
		$x = 4;
		$y = 7;
		
		$point = new Point($x, $y);
		$this->assertEquals($point->getX(), $x);
		$this->assertEquals($point->getY(), $y);
		
		$newX = 10;
		$newY = -20;
		
		$point->setX($newX);
		$point->setY($newY);
		
		$this->assertEquals($point->getX(), $newX);
		$this->assertEquals($point->getY(), $newY);
	}
	
	public function testEquals(){
		$x1 = $x2 = 55;
		$y1 = $y2 = 77;
		$point1 = new Point($x1, $y1);
		$point2 = new Point($x2, $y2);
		$point3 = new Point(1, 1);
		
		$this->assertEquals($point1, $point2);
		$this->assertNotEquals($point1, $point3);
		
		$this->assertTrue($point1->equals($point2));
		$this->assertFalse($point1->equals($point3));
	}
}