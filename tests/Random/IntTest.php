<?php
use samizdam\ValueObjects\Random\Int;

class IntTest extends PHPUnit_Framework_TestCase{
	public function testRandomValue(){
		$random = new Int(1, 2);
		$this->assertNotEquals($random->getValue(), 3);
	}
	
	public function testRandomArray(){
		$random = new Int(1, 10);
		$needNumber = 3;
		$array = $random->generateArray($needNumber);
		$this->assertCount($needNumber, $array);
		$this->assertArrayHasKey(0, $array);
		$this->assertArrayNotHasKey($needNumber, $array);
	}
}